import algorithms.GenerationGA;
import algorithms.IGeneticAlgorithm;
import algorithms.TournamentGA;
import evaluator.Evaluator;
import functions.*;
import input.Dataset;
import operators.Operators;
import operators.crossing.*;
import operators.mutation.IMutation;
import operators.mutation.SubstittueGenWithRandom;
import operators.selection.ISelection;
import operators.selection.RouletteWheelSelection;
import operators.selection.TournamentSelection;
import population.Population;
import util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {
        final int POPULATIONSIZE = 10;
        final boolean ISELITIST = true;
        final double PRECISION = 1e-7;
        final double PROBABILITYOFMUTATION = 0.01;
        final double MORTALITY = 0.5;


        String pathToFile = args[0];

        Dataset dataset = Util.readFile(pathToFile);
        Population startPopulation = new Population(POPULATIONSIZE);
        ILossFunction squareLoss = new SquaredLoss();
        IGoalFunction transferFunction = new TransferFunction();

        IError averageError = new AverageError(dataset, squareLoss, transferFunction);
        Evaluator evaluator = new Evaluator(averageError);

        ISelection roulette = new RouletteWheelSelection();
        IMutation subsMutatation = new SubstittueGenWithRandom(PROBABILITYOFMUTATION);
        List<ICrossing> crossings = new ArrayList<>();
        crossings.add(new ArithmeticRecombination());
        crossings.add(new OneFractureAndMean());
        crossings.add(new UniformCrossover());
        CrossingSelector crossingSelector = new CrossingSelector(crossings);
        Operators operatorsGenerationGA = new Operators(roulette, subsMutatation, crossingSelector);

        IGeneticAlgorithm generationGA = new GenerationGA(evaluator, ISELITIST, operatorsGenerationGA);
        generationGA.callGA(startPopulation, PRECISION );


//        Operators tournamentOperators = new Operators(new TournamentSelection(), subsMutatation, crossingSelector);
//        IGeneticAlgorithm tournament = new TournamentGA(evaluator, tournamentOperators,  MORTALITY);
//        tournament.callGA(startPopulation, PRECISION);


    }
}
