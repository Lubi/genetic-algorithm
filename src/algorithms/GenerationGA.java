package algorithms;

import chromosome.Chromosome;
import evaluator.Evaluator;
import operators.Operators;
import population.Population;
import util.Util;

import java.util.List;
import java.util.Map;

public class GenerationGA implements IGeneticAlgorithm {
    Evaluator evaluator;
    boolean isElitist;
    Operators operators;

    public GenerationGA(Evaluator evaluator, boolean isElitist, Operators operators) {
        this.evaluator = evaluator;
        this.isElitist = isElitist;
        this.operators = operators;
    }


    @Override
    public void callGA(Population population, double precision) {
        Map<Chromosome, Double> chromosomesLoss = evaluator.evaluatePopulation(population);
        double currentBestLoss = 10e8;
        int numberOfGeneration = 0;

        while(currentBestLoss > precision){
            numberOfGeneration++;
            Population newPopulation = new Population();

            if (isElitist){
                newPopulation.addChromosomeToPopulation(Util.returnBestChromosome(chromosomesLoss));
            }

            while(newPopulation.getSize() < population.getSize()){
                List<Chromosome> parents = operators.getSelection().selectParents(population, chromosomesLoss);
                Chromosome child = operators.getCross().crossover(parents.get(0), parents.get(1));
                Chromosome mutatedChild = operators.getMutation().mutate(child);
                newPopulation.addChromosomeToPopulation(mutatedChild);
            }

            population = newPopulation;
            chromosomesLoss = evaluator.evaluatePopulation(population);
            var bestChromosome = Util.returnBestChromosome(chromosomesLoss);
            var bestChromosomeLoss = chromosomesLoss.get(bestChromosome);

            if (bestChromosomeLoss < currentBestLoss){
                currentBestLoss = bestChromosomeLoss;
                System.out.println(bestChromosome + "\t" + numberOfGeneration + "\t" + currentBestLoss );
            }

        }

    }



}
