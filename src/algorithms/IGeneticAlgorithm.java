package algorithms;

import population.Population;

public interface IGeneticAlgorithm {
    void callGA(Population population, double precision);
}
