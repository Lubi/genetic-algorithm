package algorithms;

import chromosome.Chromosome;
import evaluator.Evaluator;
import operators.Operators;
import population.Population;
import util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TournamentGA implements IGeneticAlgorithm {
    Evaluator evaluator;
    Operators operators;
    double mortality;

    public TournamentGA(Evaluator evaluator, Operators operators, double mortality) {
        this.evaluator = evaluator;
        this.operators = operators;
        this.mortality = mortality;
    }

    @Override
    public void callGA(Population population, double precision) {
        Map<Chromosome, Double> chromosomesLoss = evaluator.evaluatePopulation(population);
        double currentBestLoss = 10e8;
        int numberOfGeneration = 0;
        int mortal = (int) (mortality * population.getSize());

        while (currentBestLoss > precision) {
            numberOfGeneration++;
            List<Chromosome> toAddChilds = new ArrayList<>();


            for (int i = 0; i < mortal; i++) {
                List<Chromosome> returned = operators.getSelection().selectParents(population, chromosomesLoss);
                Chromosome child = operators.getCross().crossover(returned.get(0), returned.get(1));
                Chromosome mutatedChild = operators.getMutation().mutate(child);
                population.remove(returned.get(2));
                toAddChilds.add(mutatedChild);
            }

            population.addChromosomes(toAddChilds);
            chromosomesLoss = evaluator.evaluatePopulation(population);
            var bestChromosome = Util.returnBestChromosome(chromosomesLoss);
            var bestChromosomeLoss = chromosomesLoss.get(bestChromosome);

            if (bestChromosomeLoss < currentBestLoss) {
                currentBestLoss = bestChromosomeLoss;
                System.out.println(bestChromosome + "\t" + numberOfGeneration + "\t" + currentBestLoss);
            }

        }
        ;
    }
}
