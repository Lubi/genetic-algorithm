package chromosome;

import paramaters.GAParameters;

import java.util.*;


public class Chromosome {
    List<Double> parameters = new ArrayList<>();

    public Chromosome(List<Double> params){
        this.parameters = params;
    }
    public Chromosome(int numberOfParameters) {
        randomInitParameters(numberOfParameters);
    }

    public double getParameterForIndex(int index){
        if (index < 0 || index >= getNumberOfParameters())
            throw new IndexOutOfBoundsException("Kromosom nema toliko parametara!!");
        return parameters.get(index);
    }


    public int getNumberOfParameters(){
        return parameters.size();
    }

    public void setParameterAtIndex(Double parameter, int index){
        parameters.set(index, parameter);
    }

    public List<Double> getParameters() {
        return parameters;
    }

    private void randomInitParameters(int numberOfParameters) {
        double rangeMin = GAParameters.MIN.getValue();
        double rangeMax = GAParameters.MAX.getValue();
        Random r = new Random();

        for (int i = 0; i < numberOfParameters; i++ ){
            double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            parameters.add(randomValue);
        }
    }

    @Override
    public String toString() {
        return "[" +
                parameters.get(0) + ", " +
                parameters.get(1) + ", " +
                parameters.get(2) + ", " +
                parameters.get(3) + ", " +
                parameters.get(4) +

                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chromosome that = (Chromosome) o;
        return Arrays.equals(parameters.toArray(), that.parameters.toArray());
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameters);
    }
}
