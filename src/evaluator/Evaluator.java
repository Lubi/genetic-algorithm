package evaluator;

import chromosome.Chromosome;
import functions.IError;
import population.Population;

import java.util.HashMap;
import java.util.Map;

public class Evaluator {
    IError errorFunction;

    public Evaluator(IError errorFunction) {
        this.errorFunction = errorFunction;
    }

    public Map<Chromosome, Double> evaluatePopulation(Population population){
        Map<Chromosome, Double> chromosomsEvaluated = new HashMap<>();

        for (Chromosome ch : population){
            Double error = errorFunction.getError(ch);
            chromosomsEvaluated.put(ch, error);
        }
        assert population.getSize() == chromosomsEvaluated.size();
        return chromosomsEvaluated;

    }
}
