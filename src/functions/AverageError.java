package functions;

import chromosome.Chromosome;
import input.Dataset;
import input.Example;

public class AverageError implements IError {
    Dataset dataset;
    ILossFunction lossFunction;
    IGoalFunction goalFunction;

    public AverageError(Dataset dataset, ILossFunction lossFunction, IGoalFunction goalFunction) {
        this.dataset = dataset;
        this.lossFunction = lossFunction;
        this.goalFunction = goalFunction;
    }

    @Override
    public double getError( Chromosome chromosome) {
        double sumLoss = 0;

        for (Example example: dataset){
            double predicted = goalFunction.predict(example, chromosome);
            sumLoss += lossFunction.getLoss( example.getZ() , predicted);
        }

        return sumLoss / dataset.getCardinality();
    }
}
