package functions;

import chromosome.Chromosome;

public interface IError {
    // get total (average mostly) error from given dataset, goalFunction and one chromosome
    double getError(Chromosome chromosome);
}
