package functions;

import chromosome.Chromosome;
import input.Example;

public interface IGoalFunction {
    double predict(Example example, Chromosome chromosome); // vraća h(x)
}
