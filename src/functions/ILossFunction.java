package functions;

public interface ILossFunction {
    double getLoss(double expected, double predicted);
}
