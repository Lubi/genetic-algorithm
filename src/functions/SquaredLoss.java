package functions;

public class SquaredLoss implements ILossFunction {
    @Override
    public double getLoss(double expected, double predicted) {
        return Math.pow(expected - predicted, 2); // (y -h(x) )^2
    }
}
