package functions;

import chromosome.Chromosome;
import input.Example;

public class TransferFunction implements IGoalFunction {
    @Override
    public double predict(Example example, Chromosome chromosome) {
        double x = example.getX();
        double y = example.getY();
        double b0 = chromosome.getParameterForIndex(0);
        double b1 = chromosome.getParameterForIndex(1);
        double b2 = chromosome.getParameterForIndex(2);
        double b3 = chromosome.getParameterForIndex(3);
        double b4 = chromosome.getParameterForIndex(4);

        return Math.sin(b0 + b1*x) + b2 * Math.cos(x*(b3+y)) * (1.0/(1 + Math.exp(Math.pow(x-b4,2))));
    }
}
