package input;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Dataset implements Iterable<Example>{
    private List<Example> examples = new ArrayList<>();

    public Dataset() {
    }

    public List<Example> getExamples() {
        return examples;
    }

    public void addExample(Example example){
        examples.add(example);
    }

    public int getCardinality(){
        return examples.size();
    }

    public Example elementForIndex(int index){
        if (index < 0 || index >= getCardinality()){
            throw new IndexOutOfBoundsException("Index van veličine dataseta!!");
        }
        return examples.get(index);
    }




    @Override
    public Iterator<Example> iterator() {
        return new ExampleIterator();
    }

    private class  ExampleIterator implements Iterator<Example> {
        private int currentElement =0;
        @Override
        public boolean hasNext() {
            return currentElement < getCardinality();
        }

        @Override
        public Example next() {
            return elementForIndex(currentElement++);
        }
    }
}
