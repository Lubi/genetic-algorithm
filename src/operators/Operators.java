package operators;

import operators.crossing.CrossingSelector;
import operators.crossing.ICrossing;
import operators.mutation.IMutation;
import operators.selection.ISelection;

public class Operators {
    ISelection selection;
    IMutation mutation;
    CrossingSelector crossingSelector;

    public Operators(ISelection selection, IMutation mutation, CrossingSelector crossingSelector) {
        this.selection = selection;
        this.mutation = mutation;
        this.crossingSelector = crossingSelector;
    }

    public ISelection getSelection() {
        return selection;
    }

    public IMutation getMutation() {
        return mutation;
    }

    public ICrossing getCross() {
        return crossingSelector.getRandomCrossover();
    }
}
