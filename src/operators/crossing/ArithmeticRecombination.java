package operators.crossing;

import chromosome.Chromosome;

import java.util.ArrayList;
import java.util.List;

public class ArithmeticRecombination implements ICrossing {

    @Override
    public Chromosome crossover(Chromosome first, Chromosome second) {
        int size = first.getNumberOfParameters();
        List<Double> newParams = new ArrayList<>();

        for (int i = 0; i < size; i++){
            Double firsParam = first.getParameterForIndex(i);
            Double secondParam = second.getParameterForIndex(i);

            newParams.add( (firsParam + secondParam)/ 2.0);
        }
        return new Chromosome(newParams);
    }
}
