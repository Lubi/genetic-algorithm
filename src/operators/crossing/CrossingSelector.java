package operators.crossing;

import util.Util;

import java.util.List;

public class CrossingSelector {
    List<ICrossing> crossovers;

    public CrossingSelector(List<ICrossing> crossovers) {
        this.crossovers = crossovers;
    }

    public ICrossing getRandomCrossover(){
        return crossovers.get(Util.selectRandomIndex(size()));
    }
    public int size(){
        return crossovers.size();
    }
}
