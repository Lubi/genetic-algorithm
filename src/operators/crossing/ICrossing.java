package operators.crossing;

import chromosome.Chromosome;

public interface ICrossing {
    Chromosome crossover(Chromosome  first, Chromosome second);
}
