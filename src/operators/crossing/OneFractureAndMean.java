package operators.crossing;

import chromosome.Chromosome;
import util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OneFractureAndMean implements ICrossing {
    @Override
    public Chromosome crossover(Chromosome first, Chromosome second) {
        assert first.getNumberOfParameters() == second.getNumberOfParameters();

        int size = first.getNumberOfParameters();
        int selectChromosome = new Random().nextDouble() > 0.5 ? 1: 0;


        List<Double> newGens = selectChromosome == 0 ? new ArrayList<>(first.getParameters()) : new ArrayList<>(second.getParameters());
        int fractureIndex = Util.selectRandomIndex(first.getNumberOfParameters());


        for (int i =fractureIndex; i < size; i++){
            double mean = (first.getParameterForIndex(i) + second.getParameterForIndex(i)) / 2.0;
            newGens.set(i, mean);
        }
        return new Chromosome(newGens);
    }
}
