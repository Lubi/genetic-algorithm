package operators.crossing;

import chromosome.Chromosome;
import util.Util;

import java.util.ArrayList;
import java.util.List;

public class UniformCrossover implements ICrossing {
    @Override
    public Chromosome crossover(Chromosome first, Chromosome second) {
        assert first.getNumberOfParameters() == second.getNumberOfParameters();
        int size = first.getNumberOfParameters();
        List<Integer> randVector01 = Util.generateRandomVectorO1(size);
        List<Double> newGens = new ArrayList<>();

        for (int i = 0; i < size; i++){
            if (randVector01.get(i) == 0)
                newGens.add(first.getParameterForIndex(i));
            else
                newGens.add(second.getParameterForIndex(i));
        }
        return new Chromosome(newGens);
    }
}
