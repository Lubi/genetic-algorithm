package operators.mutation;

import chromosome.Chromosome;

public interface IMutation {
    Chromosome mutate(Chromosome chromosome);
}
