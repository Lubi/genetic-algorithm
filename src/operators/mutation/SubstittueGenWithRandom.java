package operators.mutation;

import chromosome.Chromosome;
import util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SubstittueGenWithRandom implements IMutation {
    double probabilityOfMutation;

    public SubstittueGenWithRandom(double probabilityOfMutation) {
        this.probabilityOfMutation = probabilityOfMutation;
    }

    @Override
    public Chromosome mutate(Chromosome chromosome) {
        List<Double> gens = chromosome.getParameters();
        List<Double> newGens = new ArrayList<>();

        for (Double gen : gens){
            double rand = new Random().nextDouble();
            double newGen = gen;
            if (probabilityOfMutation > rand){
                newGen = Util.generateRandom(-4,4);
            }
            newGens.add(newGen);
        }

        return new Chromosome(newGens);
    }
}
