package operators.selection;

import chromosome.Chromosome;
import population.Population;

import java.util.List;
import java.util.Map;

public interface ISelection {
    List<Chromosome> selectParents(Population population, Map<Chromosome, Double> lossByChromosoms);
}
