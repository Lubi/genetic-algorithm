package operators.selection;

import chromosome.Chromosome;
import population.Population;
import util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RouletteWheelSelection  implements  ISelection{
    @Override
    public List<Chromosome> selectParents(Population population, Map<Chromosome, Double> lossByChromosoms) {
        assert population.getSize() == lossByChromosoms.size();
        List<Chromosome> parents = new ArrayList<>();
        double totalLoss = population.getPopulation().stream().map(lossByChromosoms::get).mapToDouble(d->d).sum();


        for (int i =0; i < 2; i ++){
            double randNumber = Util.generateRandom(0, population.getSize() - 1); // daj broj izmedju [0, N-1]
            double acummulative = 0;
            for (Chromosome chromosome: population) {
                acummulative += (1 - lossByChromosoms.get(chromosome) / totalLoss );
                if (acummulative >= randNumber){
                    parents.add(chromosome);
                    break;
                }
            }
        }
        assert parents.size() == 2;
        return parents;
    }
}
