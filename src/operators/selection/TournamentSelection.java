package operators.selection;

import chromosome.Chromosome;
import population.Population;
import util.Util;

import java.util.*;

public class TournamentSelection implements ISelection {
    @Override
    public List<Chromosome> selectParents(Population population, Map<Chromosome, Double> lossByChromosoms) {
        List<Chromosome> tournament = new ArrayList<>();
        double maxLoss = -10e8;
        int sizePopulation = population.getSize();
        Chromosome worstChromosome = null;

        while(tournament.size() != 3){
            Chromosome randomChromosome = population.getChromosomAtIndex(Util.selectRandomIndex(sizePopulation));
            if (!tournament.contains(randomChromosome)) {
                double lossOfChromosome = lossByChromosoms.get(randomChromosome);
                if (lossOfChromosome > maxLoss) {
                    worstChromosome = randomChromosome;
                    maxLoss = lossOfChromosome;
                }
                tournament.add(randomChromosome);
            }
        }

        tournament.remove(worstChromosome);

        List<Chromosome> returnChromos = new ArrayList<>(tournament);
        returnChromos.add(worstChromosome);

        return returnChromos;
    }
}
