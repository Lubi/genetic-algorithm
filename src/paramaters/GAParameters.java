package paramaters;

public enum GAParameters {
    MORTALITYPROBABILITY(0.5),
    MUTATIONPROBABILITY(0.01),
    MIN(-4),
    MAX(4);

    private final double value;
    GAParameters(double param) {
        value = param;
    }

    public double getValue() {
        return value;
    }
}
