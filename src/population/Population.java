package population;

import chromosome.Chromosome;
import paramaters.GAParameters;

import java.security.InvalidParameterException;
import java.util.*;

public class Population implements Iterable<Chromosome> {
    List<Chromosome> population = new ArrayList<>();

    public Population() {
    }
    public Population(Population oldPopulation){
        this.population = new ArrayList<>(oldPopulation.getPopulation());
    }

    public Population(int populationSize) {
        initPopulation(populationSize);
    }

    private void initPopulation(int populationSize) {
        Set<Chromosome> newPopulation = new HashSet<>();

        while (newPopulation.size() != populationSize){
            newPopulation.add(new Chromosome(5));
        }
        population = new ArrayList<>(newPopulation);

//        for (int i = 0; i< populationSize; i++){
//            population.add(new Chromosome(5));
//        }
    }

    public List<Chromosome> getPopulation() {
        return population;
    }

    public void addChromosomeToPopulation(Chromosome chromosome){
        population.add(chromosome);
    }
    public void remove(Chromosome chromosome){
        if (!population.contains(chromosome))
            throw new InvalidParameterException("Kromosom nije u populacioji koji zelis izbrisati!");
        population.remove(chromosome);
    }

    public void addChromosomes(List<Chromosome> chromosomes){
        population.addAll(chromosomes);
    }


    public int getSize(){
        return population.size();
    }
    public Chromosome getChromosomAtIndex(int index){
        if (index < 0 || index >= getSize())
            throw new IndexOutOfBoundsException("Populacija nema toliko kromosoma!");
        return population.get(index);
    }

    @Override
    public Iterator<Chromosome> iterator() {
        return new ChromosomeIterator();
    }

    private class ChromosomeIterator implements Iterator<Chromosome> {
        private int currentIndex = 0;
        @Override
        public boolean hasNext() {
            return currentIndex < getSize();
        }

        @Override
        public Chromosome next() {
            return getChromosomAtIndex(currentIndex++);
        }
    }
}
