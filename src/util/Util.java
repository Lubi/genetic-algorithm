package util;

import chromosome.Chromosome;
import input.Dataset;
import input.Example;

import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;

public class Util {

    public static Dataset readFile(String path) throws IOException {
        Dataset dataset = new Dataset();
        File file = new File(path);
        BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String line = null;

        while ((line = input.readLine()) != null) {
            Scanner sc = new Scanner(line);
            dataset.addExample(new Example(sc.nextDouble(), sc.nextDouble(), sc.nextDouble()));
        }

        return dataset;
    }

    public static double generateRandom(int start, int end){
        Random r = new Random();
        return start + (end - start)  *  r.nextDouble();
    }

    public static List<Integer> generateRandomVectorO1(int size){
        Random r = new Random();
        List<Integer> resultVector = new ArrayList<>();

        for (int i= 0; i< size; i++ ){
            if (r.nextDouble() > 0.5)
                resultVector.add(1);
            else
                resultVector.add(0);
        }
        return resultVector;
    }

    public static int selectRandomIndex(int sizeOfCollection){
        return new Random().nextInt(sizeOfCollection);
    }

    public static List<Integer> selectNUniqueRandomIndex(int numberOfUniqueIndexes, int sizeOfCollection){
        Set<Integer> result = new HashSet<>();
        while (result.size() != numberOfUniqueIndexes){
            result.add(selectRandomIndex(sizeOfCollection));
        }
        return new ArrayList<>(result);
    }

    public static Chromosome returnBestChromosome(Map<Chromosome, Double> chLoss){
        if (chLoss.isEmpty())
            throw new InvalidParameterException("Mapa chromosomeLoss moram imati elemente!!");

        return chLoss.entrySet().stream().min(Map.Entry.comparingByValue()).orElseThrow().getKey();


    }

}
